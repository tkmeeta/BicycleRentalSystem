<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Register</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content=""/>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="${pageContext.request.contextPath }/lib/layui/css/layui.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.login.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/fontawesome-all.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- web-fonts -->
	<!-- //web-fonts -->
</head>

<body>

	<div id="bg">
		<canvas></canvas>
		<canvas></canvas>
		<canvas></canvas>
	</div>
	<!-- //bg effect -->
	<!-- title -->
	<h1></h1>
	<br>
	<!-- //title -->
	<!-- content -->
	<div class="sub-main-w3">
		<form id='form' action="${pageContext.request.contextPath }/account/register" method="post"  enctype="multipart/form-data" >
			<div class="form-style-agile">
				<label>
					<i class="fas fa-user"></i>
					用户名
				</label>
				<input placeholder="用户名" name="accountName" type="text" class="accountName"  lay-verify="required">
				<p class="hintNo">用户名已被使用</p>
				<p class="hintYes">用户名可用</p>
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fas fa-unlock-alt"></i>
					密码
				</label>
				<input placeholder="密码" name="password" type="password" class="oldPassword" lay-verify="required">
				<p class="hintNo">密码由6-20位字母、数字、下划线组成</p>
				<p class="hintYes">密码可用</p>
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fas fa-unlock-alt"></i>
					确认密码
				</label>
				<input  placeholder="确认密码" name="accountPassword" type="password"class="newPassword" >
				<p class="hintNo">密码不一致</p>
				<p class="hintYes">密码一致</p>
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fa fa-phone-square"></i>
					手机号：
				</label>
				<input placeholder="手机号" name="phone" type="text" class="accountPhone"  >
				<p class="hintNo">手机号已重复</p>
				<p class="hintYes">手机号可用</p>
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fas fa-user"></i>
					真实姓名
				</label>
				<input placeholder="真实姓名" name="realName" type="text"  lay-verify="required">
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fa fa-clipboard"></i>
					身份证号：
				</label>
				<input placeholder="身份证号" name="idCard" type="text" class="idCard"  >
				<p class="hintNo">身份证已重复</p>
				<p class="hintYes">身份证可用</p>
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fa fa-clipboard"></i>
					身份证图片：
				</label>
				<div class="layui-upload">
				  <button type="button" class="layui-btn" id="test1">上传图片</button>
				  <div class="layui-upload-list">
				    <img class="layui-upload-img" id="demo1">
				    <p id="demoText"></p>
				  </div>
				</div>   
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fa fa-clipboard"></i>
					头像：
				</label>
				<div class="layui-upload">
				  <button type="button" class="layui-btn" id="test2">上传图片</button>
				  <div class="layui-upload-list">
				    <img class="layui-upload-img" id="demo2">
				    <p id="demoText"></p>
				  </div>
				</div>   
			</div>
			
			<!-- checkbox -->
			<div class="wthree-text">
				<ul>
					<li>
						<label class="anim">
							<input type="checkbox" class="checkbox" style=" cursor: 'not-allowed';" required="">
							<span>我已阅读并同意服务条款</span>
						</label>
					</li>
				</ul>
			</div>
			<!-- //checkbox -->
			<input type="submit" class="register" value="注册">
		</form>
	</div>
	<!-- //content -->

	<!-- copyright -->
	<div class="footer">
		<p>Copyright &copy; 2018.Company name All rights reserved.<a target="_blank" href="#">贴吧</a></p>
	</div>
	<!-- //copyright -->

	<!-- Jquery -->
	<script src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
	<script src="${pageContext.request.contextPath }/js/jquery-form.js"></script>
	<script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
	<!-- //Jquery -->
	<script type="text/javascript">
		$(function(){
			checkRegister();
			$("#form").ajaxForm(function(data){  
				if(data==true||data=='true'){
					alert("注册成功");
					window.location.href="${pageContext.request.contextPath}/jsp/account/login.jsp";
				}else{
					alert("注册失败");
				}
			});    
			$(".accountName").change(function(){
				$.post("${pageContext.request.contextPath}/account/checkAccountName",{accountName:$(this).val().trim()},function(date){
					if(date==true||date=='true'){
						$(".accountName").next().removeClass("No");
						$(".accountName").next().next().addClass("Yes");
						$(".accountName").parent().addClass("check");
					}else{
						$(".accountName").next().addClass("No");
						$(".accountName").next().next().removeClass("Yes");
						$(".accountName").parent().addClass("check");
						$(".accountName").val('');
					}
					checkRegister();
					
				});
				
			});
			
			$(".accountPhone").change(function(){
				var phone=$(".accountPhone").val().trim();
				 if(!(/^1[3456789]\d{9}$/.test(phone))){ 
						 $(".accountPhone").next().html("手机号格式有误，请重新输入！！");  
						 $(".accountPhone").next().addClass("No");
						 $(".accountPhone").next().next().removeClass("Yes");
						 $(".accountPhone").parent().addClass("check");
						 $(".accountPhone").val('');
				        return false; 
				   } 
				$.post("${pageContext.request.contextPath}/account/checkPhone",{phone:$(this).val().trim()},function(date){
					if(date==true||date=='true'){
						$(".accountPhone").next().removeClass("No");
						$(".accountPhone").next().next().addClass("Yes");
						$(".accountPhone").parent().addClass("check");
					}else{
						$(".accountPhone").next().html("手机号已被注册"); 
						$(".accountPhone").next().addClass("No");
						$(".accountPhone").next().next().removeClass("Yes");
						$(".accountPhone").parent().addClass("check");
						$(".accountPhone").val('');
					}
					checkRegister();
					
				});
				
			});
			$(".idCard").change(function(){
				var idCard=$(".idCard").val().trim();
				 if(!(/^\d{17}[\d|X]$|^\d{15}$/.test(idCard))){ 
						 $(".idCard").next().html("身份证格式有误，请重新输入！！");  
						 $(".idCard").next().addClass("No");
						 $(".idCard").next().next().removeClass("Yes");
						 $(".idCard").parent().addClass("check");
						 $(".idCard").val('');
				        return false; 
				   } 
				$.post("${pageContext.request.contextPath}/account/checkIdCard",{idCard:$(this).val().trim()},function(date){
					if(date==true||date=='true'){
						$(".idCard").next().removeClass("No");
						$(".idCard").next().next().addClass("Yes");
						$(".idCard").parent().addClass("check");
					}else{
						$(".idCard").next().html("身份证已被占用"); 
						$(".idCard").next().addClass("No");
						$(".idCard").next().next().removeClass("Yes");
						$(".idCard").parent().addClass("check");
						$(".idCard").val('');
					}
					checkRegister();
				});
				
			});
			
			$(".newPassword").change(function(){
				var old=$(".oldPassword").val();
				var password=$(".newPassword").val();
				if(old==password){
					$(".newPassword").next().removeClass("No");
					$(".newPassword").next().next().addClass("Yes");
					$(".newPassword").parent().addClass("check");
				}else{
					$(".newPassword").next().addClass("No");
					$(".newPassword").next().next().removeClass("Yes");
					$(".newPassword").parent().addClass("check");
					$(".newPassword").val('');
				}
				checkRegister();
			
			});
			
			$(".oldPassword").change(function(){
				var old=$(".oldPassword").val();
				if(/^(\w){6,20}$/.test(old)){
					$(".oldPassword").next().removeClass("No");
					$(".oldPassword").next().next().addClass("Yes");
					$(".oldPassword").parent().addClass("check");
				}else{
					$(".oldPassword").next().addClass("No");
					$(".oldPassword").next().next().removeClass("Yes");
					$(".oldPassword").parent().addClass("check");
					$(".oldPassword").val('');
				}
				
			});
			function checkRegister(){
				var num=$(".Yes").length;
				if(num==5){
					$(".register").css({
						cursor:"pointer"
					});
				}else{
					$(".register").css({
						 cursor: "not-allowed"
					});
				}
			}
			
			
				
});
	</script>
	<!-- effect js -->
	<script src="${pageContext.request.contextPath }/js/canva_moving_effect.js"></script>
	<!-- //effect js -->
	<script>
	layui.use('upload', function(){
	  var upload = layui.upload;
	   
	  upload.render({
		  elem: '#test1'
		  ,auto: false //选择文件后不自动上传
		  ,choose: function(obj){
		  		//预读本地文件示例，不支持ie8
		      obj.preview(function(index, file, result){
		        $('#demo1').attr('src', result); //图片链接（base64）
		      });
		  }
		});      
	  upload.render({
		  elem: '#test2'
		  ,auto: false //选择文件后不自动上传
		  ,choose: function(obj){
		  		//预读本地文件示例，不支持ie8
		      obj.preview(function(index, file, result){
		        $('#demo2').attr('src', result); //图片链接（base64）
		      });
		  }
		});      
	});
</script>

</body>

</html>