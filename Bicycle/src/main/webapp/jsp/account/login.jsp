<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content=""/>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.login.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/fontawesome-all.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
</head>

<body>
	<!-- bg effect -->
	<div id="bg">
		<canvas></canvas>
		<canvas></canvas>
		<canvas></canvas>
	</div>
	<!-- //bg effect -->
	<!-- title -->
	<h1></h1>
	<br>
	<!-- //title -->
	<!-- content -->
	<div class="sub-main-w3" >
		<form id='form' action="${pageContext.request.contextPath }/account/login" method="post" target="target">
			<div class="form-style-agile">
				<label>
					<i class="fas fa-user"></i>
					用户名
				</label>
				<input class="Name" placeholder="用户名" name="accountName" type="text" required="">
			</div>
			<div class="form-style-agile">
				<label>
					<i class="fas fa-unlock-alt"></i>
					密码
				</label>
				<input class="Password" placeholder="密码" name="accountPassword" type="password" required="">
			</div>
			<!-- checkbox -->
			<div class="wthree-text">
				<ul>
					<li>
						<label class="anim">
							<input type="checkbox" class="checkbox" required="">
							<span>我已阅读并同意服务条款</span>
						</label>
					</li>
					<li>
						<a href="#">忘记密码</a>|<a href="${pageContext.request.contextPath }/jsp/account/register.jsp">注册</a>
					</li>
				</ul>
			</div>
			<!-- //checkbox -->
			<input type="submit" class="login" value="登录">
		</form>
		<iframe name="target" style="display:none" ></iframe>
	</div>
	<!-- //content -->

	<!-- copyright -->
	<div class="footer">
		<p>Copyright &copy; 2018.Company name All rights reserved.<a target="_blank" href="#">自行车租赁网</a></p>
	</div>
	<!-- //copyright -->

	<!-- Jquery -->
	<script src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
	<script src="${pageContext.request.contextPath }/js/jquery-form.js"></script>
	<!-- //Jquery -->
	<script type="text/javascript">
		$(function(){
			$("#form").ajaxForm(function(data){  
				if(data==true||data=='true'){
					alert("登录成功");
					//window.location.href="${pageContext.request.contextPath}/jsp/admin/index.jsp";
				}else{
					alert("登录失败");
					$(".Name").val('');
					$(".Password").val('');
					
				}
			}); 
			
		});
	</script>
	<!-- effect js -->
	<script src="${pageContext.request.contextPath }/js/canva_moving_effect.js"></script>
	<!-- //effect js -->

</body>

</html>