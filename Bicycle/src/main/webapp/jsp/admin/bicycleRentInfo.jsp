<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>租车信息</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
        <script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
    </head>
    <script type="text/javascript">
   	var bicycleId="${param.bicycleId}";
   	function showRentCar(){
   		var startDate=$("#start").val();
   		var expiryDate=$("#end").val();
   		console.log(bicycleId);
   		var param={
   				bicycleId:bicycleId,
   				startDate:startDate,
   				expiryDate:expiryDate
   		}
   		$.post("${pageContext.request.contextPath}/rentCar/getRentCarByQuery",param,function(data){
   			console.log(data);
   			var rentCars=eval(data);
   			var tr="";
   			for(var i=0 ; i<rentCars.length; i++){
   				var state='';
   				var str='';
   				if(rentCars[i].state==1){
   					state='正在使用';
   					str="<button type='button' class='layui-btn layui-btn-radius layui-btn-normal returnCar' rId='"+rentCars[i].rentId+"'>立即还车</button>";
   				}else{
   					state="已支付";
   					/* str="<button type='button' class='layui-btn layui-btn-radius layui-btn-warm state1'>立即使用</button> "; */
   				}
   				tr+="<tr><td>"+rentCars[i].account.accountName+
   				"</td><td>"+rentCars[i].bicycle.bicycleNumber+
   				"</td><td>"+rentCars[i].startDate+
   				"</td><td>"+rentCars[i].expiryDate+
   				"</td><td> "+state+
   				"</td><td>"+str+"</td></tr>";
   			}
   			$(".rentCar-body").html(tr);
   		});
   	}
    $(function(){
    	
    	showRentCar();
    	
    	$(document).on("click",".returnCar",function(){
				var rentId=$(this).attr("rId");
				$.post("${pageContext.request.contextPath}/rentCar/doReturnCar",{rentId:rentId},function(data){
					if(data==true||data=='true'){
						alert("还车成功");
					}else{
						alert("还车失败");
					}
					showRentCar();
				});
    	});
    	
    });
    </script>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>租车信息</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="开始日" name="start" id="start" type="text" autocomplete="off"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="截止日" name="end" id="end" type="text" autocomplete="off"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit lay-filter="sreach">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        <th>使用人</th>
                                        <th>自行车编号</th>
                                        <th>租用时间</th>
                                        <th>还车时间</th>
                                        <th>订单状态</th>
                                        <th>管理</th>
                                        </tr>
                                </thead>
                                <tbody class="rentCar-body">
                                </tbody>
                            </table>
                        </div>
                        <!-- <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <a class="prev" href="">&lt;&lt;</a>
                                    <a class="num" href="">1</a>
                                    <span class="current">2</span>
                                    <a class="num" href="">3</a>
                                    <a class="num" href="">489</a>
                                    <a class="next" href="">&gt;&gt;</a></div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' 
                ,type: 'datetime'
                ,trigger: 'click'//指定元素
            });
            //执行一个laydate实例
            laydate.render({
                elem: '#end',
               	type: 'datetime'//指定元素
               	 ,trigger: 'click'
            });
            
            var form = layui.form;
            
            form.on("submit(sreach)",function(data){
            	showRentCar();
            	return false;
            });
            
        });
        </script>

</html>