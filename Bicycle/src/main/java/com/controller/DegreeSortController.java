package com.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.DegreeSort;
import com.service.DegreeSortService;

@Controller
@RequestMapping(value = "/degreeSort")
public class DegreeSortController {

	@Reference
	private DegreeSortService degreeSortService;
	
	@ResponseBody
	@RequestMapping(value = "/getDegreeSortAll" )
	public List<DegreeSort> getDegreeSortAll(){
		return this.degreeSortService.getDegreeSortAll();
	}
	
	@ResponseBody
	@RequestMapping(value = "/doInsertDegreeSort" )
	public boolean doInsertDegreeSort(DegreeSort degreeSort) {
		return this.degreeSortService.doInsertDegreeSort(degreeSort);
	}
	
	@ResponseBody
	@RequestMapping(value = "/doUpdateDegreeSort" )
	public boolean doUpdateDegreeSort(DegreeSort degreeSort) {
		return this.degreeSortService.doUpdateDegreeSort(degreeSort);
	}
	
	@ResponseBody
	@RequestMapping(value = "/doDeleteDegreeSort" )
	public boolean doDeleteDegreeSort(Integer degreeId) {
		return this.degreeSortService.doDeleteDegreeSort(degreeId);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getDegreeSortByDegreeId" )
	public DegreeSort getDegreeSortByDegreeId(Integer degreeId) {
		return this.degreeSortService.getDegreeSortByDegreeId(degreeId);
	}
}
