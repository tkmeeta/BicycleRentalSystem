package com.controller;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.TimeSort;
import com.service.TimeSortService;

@Component
@RequestMapping(value = "/timeSort")
public class TimeSortController {
	@Reference
	private TimeSortService timeSortService;
	
	@ResponseBody
	@RequestMapping(value = "/getTimeSortAll")
	public List<TimeSort> getTimeSortAll(){
		return this.timeSortService.getTimeSortAll();
	}
	
	@ResponseBody
	@RequestMapping(value = "/doInsertTimeSort")
	public boolean doInsertTimeSort(TimeSort TimeSort) {
		return this.timeSortService.doInsertTimeSort(TimeSort);
	}
	
	@ResponseBody
	@RequestMapping(value = "/doUpdateTimeSort")
	public boolean doUpdateTimeSort(TimeSort TimeSort) {
		return this.timeSortService.doUpdateTimeSort(TimeSort);
	}
	
	@ResponseBody
	@RequestMapping(value = "/doDeleteTime")
	public boolean doDeleteTimeSort(Integer timeId) {
		return this.timeSortService.doDeleteTimeSort(timeId);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTimeSortByTimeId")
	public TimeSort getTimeSortByTimeId(Integer timeId) {
		return this.timeSortService.getTimeSortByTimeId(timeId);
	}
}
