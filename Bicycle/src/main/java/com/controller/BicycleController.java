package com.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.Bicycle;
import com.service.BicycleService;

@Controller
@RequestMapping(value = "/bicycle")
public class BicycleController {
	
	@Reference
	private BicycleService bicycleService;
	
	@ResponseBody
	@RequestMapping(value = "/getBicycleByQueryCondition")
	public List<Bicycle> getBicycleByQueryCondition(String populationIds, String degreeIds ,String timeIds ,Double priceMin ,Double priceMax ,String brand ,String states ){
		return this.bicycleService.getBicycleByQueryCondition(populationIds, degreeIds, timeIds, priceMin, priceMax, brand, states);
	}
	
	@ResponseBody
	@RequestMapping(value="/doInsertBicycle")
	public boolean  doInsertBicycle(Bicycle bicycle ) {
		return this.bicycleService.doInsertBicycle(bicycle);
	}
	@ResponseBody
	@RequestMapping(value="/doUpdateBicycle")
	public boolean  doUpdateBicycle(Bicycle bicycle) {
		return this.bicycleService.doUpdateBicycle(bicycle);
	}
	
	@ResponseBody
	@RequestMapping(value="/doDeleteBicycle")
	public boolean  doDeleteBicycle(Integer bicycleId) {
		return this.bicycleService.doDeleteBicycle(bicycleId);
	}
	
	@ResponseBody
	@RequestMapping(value="/getBicycleByBicycleId")
	public Bicycle  getBicycleByBicycleId(Integer bicycleId) {
		return this.bicycleService.getBicycleByBicycleId(bicycleId);
	}
	
	@ResponseBody
	@RequestMapping(value="/doUpdateStateByBicycleIdAndState")
	public boolean  doUpdateStateByBicycleIdAndState(Integer bicycleId ,Integer state) {
		return this.bicycleService.doUpdateStateByBicycleIdAndState(bicycleId, state);
	}
	
	
}
