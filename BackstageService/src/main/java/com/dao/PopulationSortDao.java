package com.dao;

import java.util.List;

import com.entities.PopulationSort;

public interface PopulationSortDao {
	//获取所有的人数分类信息
	public List<PopulationSort> getPopulationSort();
	
	//添加新的分类
	public void doInsertPopulationSort(PopulationSort populationSort);
	
	//修改分类信息
	public void doUpdatePopulationSort(PopulationSort populationSort);
	
	//通过id获取分类信息
	public PopulationSort getPopulationByPopulationId(Integer populationId);
	
	//删除分类
	public void doDeletePopulationSort(Integer populationId);
}
