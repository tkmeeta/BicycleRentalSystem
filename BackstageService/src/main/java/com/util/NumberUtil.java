package com.util;

import net.sourceforge.pinyin4j.PinyinHelper;

public class NumberUtil {
	public static String getBicycleNumberPrefix(String brand) {
		String prefix="";
		StringBuilder convert = new StringBuilder();
        for (int j = 0; j < brand.length(); j++) {
            char word = brand.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert.append(pinyinArray[0].charAt(0));
            } else {
                convert.append(word);
            }
        }
        prefix=convert.toString().toUpperCase();
		return prefix;
	}
}
