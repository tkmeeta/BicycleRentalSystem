package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.PopulationSortDao;
import com.entities.PopulationSort;
import com.service.PopulationSortService;

@Component
@Service
public class PopulationSortServiceImp implements PopulationSortService{
	
	@Autowired 
	private PopulationSortDao populationSortDao;

	@Transactional(readOnly =  true)
	@Override
	public List<PopulationSort> getPopulationSortAll() {
		return this.populationSortDao.getPopulationSort();
	}
	@Transactional
	@Override
	public boolean doInsertPopulationSort(PopulationSort populationSort) {
		boolean flag=false;
		try {
			this.populationSortDao.doInsertPopulationSort(populationSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Transactional
	@Override
	public boolean doUpdatePopulationSort(PopulationSort populationSort) {
		boolean flag=false;
		try {
			this.populationSortDao.doUpdatePopulationSort(populationSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Transactional(readOnly =  true)
	@Override
	public PopulationSort getPopulationByPopulationId(Integer populationId) {
		return this.populationSortDao.getPopulationByPopulationId(populationId);
	}
	@Override
	public boolean doDeletePopulationSort(Integer populationId) {
		boolean flag=false;
		try {
			this.populationSortDao.doDeletePopulationSort(populationId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	
}
