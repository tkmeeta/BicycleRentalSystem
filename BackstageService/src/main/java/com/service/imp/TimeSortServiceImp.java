package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.TimeSortDao;
import com.entities.TimeSort;
import com.service.TimeSortService;

@Service
@Component
public class TimeSortServiceImp implements TimeSortService {
	@Autowired
	private TimeSortDao timeSortDao;
	
	@Transactional(readOnly =  true)
	@Override
	public List<TimeSort> getTimeSortAll() {
		return this.timeSortDao.getTimeSortAll();
	}
	
	@Transactional()
	@Override
	public boolean doInsertTimeSort(TimeSort timeSort) {
		boolean flag=false;
		try {
			this.timeSortDao.doInsertTimeSort(timeSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional()
	@Override
	public boolean doUpdateTimeSort(TimeSort timeSort) {
		boolean flag=false;
		try {
			this.timeSortDao.doUpdateTimeSort(timeSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional(readOnly =  true)
	@Override
	public TimeSort getTimeSortByTimeId(Integer timeId) {
		return this.timeSortDao.getTimeSortByTimeId(timeId);
	}

	@Transactional()
	@Override
	public boolean doDeleteTimeSort(Integer timeId) {
		boolean flag=false;
		try {
			this.timeSortDao.doDeleteTimeSort(timeId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

}
