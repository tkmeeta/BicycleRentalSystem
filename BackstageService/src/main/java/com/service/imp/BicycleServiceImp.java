package com.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.BicycleDao;
import com.entities.Bicycle;
import com.entities.BicyclePrice;
import com.service.BicyclePriceService;
import com.service.BicycleService;
import com.util.NumberUtil;

@Service
@Component
public class BicycleServiceImp implements BicycleService {
	@Autowired
	private BicycleDao bicycleDao;
	
	@Autowired
	private BicyclePriceService bicyclePriceService;
	
	@Override
	@Transactional(readOnly = true)
	public List<Bicycle> getBicycleByQueryCondition(String populationIds, String degreeIds, String timeIds,
			Double priceMin,Double priceMax , String brand, String states) {
		List<BicyclePrice> prices=this.bicyclePriceService.getBicyclePriceByPopulationIdsAndDegreeIdsAndTimeIdsAndPriceMaxAndPriceMin(populationIds, degreeIds, timeIds, priceMin, priceMax);
		List<Integer> priceIds =new ArrayList<Integer>();
		for (BicyclePrice bicyclePrice : prices) {
			priceIds.add(bicyclePrice.getPriceId());
		}
		if(priceIds.size()==0) {
			priceIds.add(0);
		}
		String []state=null;
		if (null != states && !("".equals(states))) {
			state=states.split(",");
		}
		return this.bicycleDao.getBicycleByPriceIdsAndLikeBand(priceIds, "%"+brand+"%",state);
	}
	
	@Transactional()
	@Override
	public boolean doInsertBicycle(Bicycle bicycle) {
		boolean flag=false;
		//获取品牌前缀
		String prefix =NumberUtil.getBicycleNumberPrefix(bicycle.getBrand());
		String MaxNumber=this.bicycleDao.getBicycleNumberByBrand(bicycle.getBrand());
		if(MaxNumber !=null) {
			MaxNumber=MaxNumber.substring(prefix.length());
			bicycle.setBicycleNumber(prefix+String.format("%05d",Integer.parseInt( MaxNumber)+1));
		}else {
			bicycle.setBicycleNumber(prefix+"00001");
		}
		this.bicycleDao.doInsertBicycle(bicycle);
		flag=true;
		return flag;
	}
	
	@Transactional()
	@Override
	public boolean doUpdateBicycle(Bicycle bicycle) {
		boolean flag=false;
		try {
			if(!bicycle.getBrand().equals(this.bicycleDao.getBicycelByBicycleId(bicycle.getBicycleId()).getBrand())) {
			//获取品牌前缀
				String prefix =NumberUtil.getBicycleNumberPrefix(bicycle.getBrand());
				String MaxNumber=this.bicycleDao.getBicycleNumberByBrand(bicycle.getBrand());
				if(MaxNumber !=null) {
					MaxNumber=MaxNumber.substring(prefix.length());
					bicycle.setBicycleNumber(prefix+String.format("%05d",Integer.parseInt( MaxNumber)+1));
				}else {
					bicycle.setBicycleNumber(prefix+"00001");
				}
			}
			this.bicycleDao.doUpdateBicycle(bicycle);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Transactional()
	@Override
	public boolean doDeleteBicycle(Integer bicycleId) {
		boolean flag=false;
		try {
			this.bicycleDao.doDeleteBicycle(bicycleId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public Bicycle getBicycleByBicycleId(Integer bicycleId) {
		return this.bicycleDao.getBicycelByBicycleId(bicycleId);
	}

	@Override
	public boolean doUpdateStateByBicycleIdAndState(Integer bicycleId, Integer state) {
		boolean flag=false;
		try {
			this.bicycleDao.doUpdateStateByBicycleIdAndState(bicycleId, state);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

}
