package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.BicyclePriceDao;
import com.entities.BicyclePrice;
import com.service.BicyclePriceService;

@Service
@Component
public class BicyclePriceServiceImp implements BicyclePriceService {
	@Autowired
	private BicyclePriceDao bicyclePriceDao;
	
	@Transactional(readOnly = true)
	@Override
	public List<BicyclePrice> getBicyclePriceByPopulationIdsAndDegreeIdsAndTimeIdsAndPriceMaxAndPriceMin(
			String populationIds, String degreeIds, String timeIds, Double priceMin, Double priceMax) {
		String [] pids=null;
		String [] dids=null;
		String [] tids=null;
		if (null != populationIds && !("".equals(populationIds))) {
			pids=populationIds.split(",");
		}
		if(null != degreeIds && !("".equals(degreeIds))) {
			dids=degreeIds.split(",");
		}
		if(null != timeIds && !("".equals(timeIds))) {
			tids=timeIds.split(",");
		}
		if(null != priceMin && null != priceMax) {
			Double max=Math.max(priceMin, priceMax);
			Double min=Math.min(priceMin, priceMax);
			priceMin=min;
			priceMax=max;
		}
		return this.bicyclePriceDao.getBicyclePriceByPopulationIdsAndDegreeIdsAndTimeIdsAndPriceMaxAndPriceMin(pids, dids, tids, priceMin, priceMax);
	}

	@Transactional()
	@Override
	public boolean doInsertBicyclePrice(BicyclePrice bicyclePrice) {
		boolean flag=false;
		try {
			this.bicyclePriceDao.doInsertBicyclePrice(bicyclePrice);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional()
	@Override
	public boolean doUpdateBicyclePrice(BicyclePrice bicyclePrice) {
		boolean flag=false;
		try {
			this.bicyclePriceDao.doUpdateBicyclePrice(bicyclePrice);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional(readOnly = true)
	@Override
	public BicyclePrice getBicyclePriceByPriceId(Integer priceId) {
		return this.bicyclePriceDao.getBicyclePriceByPriceId(priceId);
	}
	
	@Transactional()
	@Override
	public boolean doDeleteBicyclePrice(Integer priceId) {
		boolean flag=false;
		try {
			this.bicyclePriceDao.doDeleteBicyclePrice(priceId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional(readOnly = true)
	@Override
	public BicyclePrice getBicyclePriceByBicyclePrice(BicyclePrice bicyclePrice) {
		BicyclePrice Price=null;
		Price =this.bicyclePriceDao.getBicyclePriceByBicyclePrice(bicyclePrice);
		return Price;
	}

}
