package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.CouponsDao;
import com.entities.Coupons;
import com.service.CouponsService;

@Service
@Component
public class CouponsServiceImp implements CouponsService {
	
	@Autowired
	private CouponsDao couponsDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Coupons> getCouponsBuQuery(String startDate, String expiryDate, Double priceMin, Double priceMax) {
		if(null != priceMin && null != priceMax) {
			Double max=Math.max(priceMin, priceMax);
			Double min=Math.min(priceMin, priceMax);
			priceMin=min;
			priceMax=max;
		}
		return this.couponsDao.getCouponsByQuery(startDate, expiryDate, priceMin, priceMax);
	}

	@Transactional()
	@Override
	public boolean doInsertCoupons(Coupons coupons) {
		boolean flag=false;
		try {
			this.couponsDao.doInsertCoupons(coupons);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional()
	@Override
	public boolean doUpdateCoupons(Coupons coupons) {
		boolean flag=false;
		try {
			this.couponsDao.doUpdateCoupons(coupons);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional(readOnly = true)
	@Override
	public Coupons getCouponsByCouponsId(Integer couponsId) {
		return this.couponsDao.getCouponsByCouponsId(couponsId);
	}

	@Transactional()
	@Override
	public boolean doDeleteCoupons(Integer couponsId) {
		boolean flag=false;
		try {
			this.couponsDao.doDeleteCoupons(couponsId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

}
