package com.dao;

import com.entities.Account;

public interface AccountDao {
	//登录
	public Account loginByAccountNameAndAccountPassword(String accountName ,String accountPassword);
	
	//检查用户名是否可用
	public Integer checkAccountName(String accountName);
	
	//检查手机号是否可用
	public Integer checkPhone(String phone);
	
	//检查身份证号是否可用
	public Integer checkIdCard(String idCard);
	
	//用户注册
	public void registerAccount(Account account);
}
