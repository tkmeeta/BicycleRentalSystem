package com.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class BicyclePrice implements Serializable {
	private Integer priceId;
	private Integer populationId;
	private Integer degreeId;
	private Integer timeId;
	private Double price;
	private PopulationSort populationSort;
	private DegreeSort degreeSort;
	private TimeSort timeSort;
	
	public Integer getPopulationId() {
		return populationId;
	}
	public void setPopulationId(Integer populationId) {
		this.populationId = populationId;
	}
	public PopulationSort getPopulationSort() {
		return populationSort;
	}
	public void setPopulationSort(PopulationSort populationSort) {
		this.populationSort = populationSort;
	}
	public DegreeSort getDegreeSort() {
		return degreeSort;
	}
	public void setDegreeSort(DegreeSort degreeSort) {
		this.degreeSort = degreeSort;
	}
	public TimeSort getTimeSort() {
		return timeSort;
	}
	public void setTimeSort(TimeSort timeSort) {
		this.timeSort = timeSort;
	}
	public Integer getPriceId() {
		return priceId;
	}
	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}
	public Integer getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(Integer degreeId) {
		this.degreeId = degreeId;
	}
	public Integer getTimeId() {
		return timeId;
	}
	public void setTimeId(Integer timeId) {
		this.timeId = timeId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
