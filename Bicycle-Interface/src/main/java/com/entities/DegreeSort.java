package com.entities;

import java.io.Serializable;

public class DegreeSort implements Serializable{
	private Integer degreeId;
	private String degreeName;
	public Integer getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(Integer degreeId) {
		this.degreeId = degreeId;
	}
	public String getDegreeName() {
		return degreeName;
	}
	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}
	
}
