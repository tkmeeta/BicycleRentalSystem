package com.entities;

import java.io.Serializable;

public class Coupons implements Serializable{
	private Integer couponsId;
	private Double price;
	private Double astrictPrice;
	private Integer number;
	private String startDate;
	private String expiryDate;
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Integer getCouponsId() {
		return couponsId;
	}
	public void setCouponsId(Integer couponsId) {
		this.couponsId = couponsId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAstrictPrice() {
		return astrictPrice;
	}
	public void setAstrictPrice(Double astrictPrice) {
		this.astrictPrice = astrictPrice;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
}
