package com.service;

import java.util.List;

import com.entities.RentCar;

public interface RentCarService {
	//通过自行车id，起始时间，还车时间查询
	public List<RentCar> getRentCarByBicycleIdAndStartDateAndExpiryDate(Integer bicycleId,String startDate ,String expiryDate);
	
	//立即还车
	public boolean doReturnCar(Integer rentId);
}
