package com.service;

import com.entities.Account;

public interface AccountService {
	//登录
	public Account loginAccount(String accountName,String accountPassword);
	
	//检查用户名是否可用
	public boolean checkAccountName(String accountName);
	
	//检查手机号是否可用
	public boolean checkPhone(String phone);
	
	//检查身份证号是否可用
	public boolean checkIdCard(String idCard);
	
	//用户注册
	public boolean registerAccount(Account account);
}
