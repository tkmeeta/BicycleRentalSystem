package com.service;

import java.util.List;

import com.entities.DegreeSort;

public interface DegreeSortService {
	//显示所有的程度分类信息
	public List<DegreeSort> getDegreeSortAll();
	
	//添加新的分类
	public boolean doInsertDegreeSort(DegreeSort degreeSort);
	
	//修改分类信息
	public boolean doUpdateDegreeSort(DegreeSort degreeSort);
	
	//通过id获取分类信息
	public DegreeSort getDegreeSortByDegreeId(Integer degreeId);
	
	//通过id来删除分类
	public boolean doDeleteDegreeSort(Integer degreeId);
}
