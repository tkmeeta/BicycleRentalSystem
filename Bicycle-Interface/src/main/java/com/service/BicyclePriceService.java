package com.service;

import java.util.List;

import com.entities.BicyclePrice;

public interface BicyclePriceService {
	//查询价格表信息
	public List<BicyclePrice> getBicyclePriceByPopulationIdsAndDegreeIdsAndTimeIdsAndPriceMaxAndPriceMin(String populationIds,String degreeIds,String timeIds ,Double priceMin,Double priceMax);
	
	//添加新的价格
	public boolean doInsertBicyclePrice(BicyclePrice bicyclePrice);
	
	//修改价格
	public boolean doUpdateBicyclePrice(BicyclePrice bicyclePrice);
	
	//通过id获取价格信息
	public BicyclePrice getBicyclePriceByPriceId(Integer priceId);
	
	//删除价格
	public boolean doDeleteBicyclePrice(Integer priceId);
	
	//通过BicyclePrice获取价格信息
	public BicyclePrice getBicyclePriceByBicyclePrice(BicyclePrice bicyclePrice);
	
}
