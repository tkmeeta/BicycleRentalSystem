package com.service;

import java.util.List;

import com.entities.PopulationSort;

public interface PopulationSortService {
	//显示所有的人数类型
	public List<PopulationSort> getPopulationSortAll();

	//添加新的分类
	public boolean doInsertPopulationSort(PopulationSort populationSort);
	
	//修改分类信息
	public boolean doUpdatePopulationSort(PopulationSort populationSort);
	
	//通过id获取分类信息
	public PopulationSort getPopulationByPopulationId(Integer populationId);
	
	//通过id来删除分类
	public boolean doDeletePopulationSort(Integer populationId);
}
